ythesis
================================================================================

[![pipeline status](https://gitlab.com/yassu/ythesis.py/badges/master/pipeline.svg)](https://gitlab.com/yassu/ythesis.py/-/commits/master)

This library provides Thesis class, which is a frozen dataclass.
