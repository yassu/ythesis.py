from datetime import datetime

from ythesis import Thesis


def test_init():
    Thesis(
        title='abc',
        authors=tuple(['abc']),
        published=datetime(2000, 1, 1),
        journal_name='',
        url='')


def test_convert_set():
    {
        Thesis(
            title='abc',
            authors=tuple(['abc']),
            published=datetime(2000, 1, 1),
            journal_name='',
            url='')
    }


def test_convert_dict():
    {
        'abc':
            Thesis(
                title='abc',
                authors=tuple(['abc']),
                published=datetime(2000, 1, 1),
                journal_name='',
                url='')
    }
